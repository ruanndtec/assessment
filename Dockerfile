FROM ubuntu:20.04

ENV TZ=Africa/Johannesburg
ENV DEBIAN_FRONTEND noninteractive
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /var/www/html

# Copy our index.php containing the phpinfo command
COPY index.php index.php

# Copy apache config
COPY test.ruanfourie.com.conf /etc/apache2/sites-available/test.ruanfourie.com.conf

# Ignore suggested and recommended packages to lighten our image
RUN echo 'APT::Install-Suggests "0";' >> /etc/apt/apt.conf.d/00-docker
RUN echo 'APT::Install-Recommends "0";' >> /etc/apt/apt.conf.d/00-docker

RUN apt-get update && \
    apt-get -y install apache2 apache2-utils libapache2-mod-php php7.4 && \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf && \
    a2dissite 000-default && \
    a2ensite test.ruanfourie.com.conf && \
    service apache2 restart

EXPOSE 80

CMD ["apachectl", "-D", "FOREGROUND"]